mongoose = require('mongoose')
Schema = mongoose.Schema

MemberSchema = require('./member').MemberSchema
MessageSchema = require('./message').MessageSchema

RoomSchema= new Schema
  name: String
  desc: String
  limit: {type: Number, default: 12, min: 5, max: 30}
  time:
    date: {type: Number, default: 10, min: 1, max: 30}
    night: {type: Number, default: 10, min: 1, max: 30}
  organizer: {type: Schema.ObjectId, ref: 'User'}
  users: [MemberSchema]
  current_stage: {type: Number, default: 0}
  status: {type: Number, default: 0}
  messages: [ MessageSchema ]
  created_at: {type: Date, default: Date.now }
  updated_at: {type: Date, default: Date.now }

RoomSchema.pre 'save', (next)->
  @updated_at = (new Date()).getTime()
  next()

mongoose.model 'Room', RoomSchema

module.exports.Room = mongoose.model 'Room'
module.exports.RoomSchema = RoomSchema