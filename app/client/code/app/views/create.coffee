class App.views.create extends App.Page
  template: 'create-index'
  render: =>
    super
    @$form = $('#create-form')

    @fields =
      name: @$form.find('#create-name')
      desc: @$form.find('#create-description')
      limit: @$form.find('#create-limit')
      time:
        date: @$form.find('#create-time-date')
        night: @$form.find('#create-time-night')

    @$form.off('submit').on('submit', @send)

  get_param: =>
    name: @fields.name.val()
    desc: @fields.desc.val()
    limit: @fields.limit.val()
    time:
      date: @fields.time.date.val()
      night: @fields.time.night.val()

  send: =>
    params = @get_param()
    ss.rpc('room.create', params, (result, errors = null)=>
      return @set_errors(errors) if !result

      router.move('room')
    )
    false

  set_errors: (errors)=>
    #TODO いつかやる
    console.log('set_errors', errors)
