class App.views.room extends App.Page
  template: 'room-index'
  limit: 600

  jobs_list: '''
             <ul class="unstyled inline margin0">
               <li><i class="icon-leaf"></i> 人狼：2人</li>
               <li><i class="icon-user"></i> 村人：10人</li>
               <li><i class="icon-eye-open"></i> 占い師：2人</li>
               <li><i class="icon-search"></i> 霊能者：1人</li>
               <li><i class="icon-flag"></i> 狩人：2人</li>
               <li><i class="icon-headphones"></i> 共有者：2人</li>
             </ul>
             '''

  message: _.template '''
  <hr>
  <div class="media">
    <img class="media-object user-icon-mini" src="<%= icon %>">
    <div class="media-body">
      <p><strong><%= name %></strong></p>
      <%= message %>
    </div>
  </div>
                      '''

  render: =>
    super()
    $('#jobs-list').popover
      html: true
      trigger: 'hover'
      placement: 'left'
      title: '今回のゲームで登場する役職'
      content: @jobs_list

    @$html = $('html')
    @$timer = $('#timer')
    @$bar = @$timer.find('.bar')

    timer = null
    init_timer = (option_time = @limit)=>
      time = option_time
      current = time

      @$bar.text(time).css('width', '100%')
      @$timer.removeClass('progress-warning').removeClass('progress-danger')

      clearInterval(timer) if timer isnt null

      timer = setInterval(=>
        current--
        percent = (current / time) * 100

        @$timer.addClass('progress-warning') if percent <= 50
        @$timer.removeClass('progress-warning').addClass('progress-danger') if percent <= 10

        @$bar.text(current).css('width', percent.toString() + '%')

        if current <= 0
          clearInterval(timer)
          if @$html.hasClass('dark')
            @$html.removeClass('dark')
          else
            @$html.addClass('dark')
          init_timer()
      , 1000)

    init_timer()

    @$enquete = $('#enquete')
    @$chat = $('#chat')
    @$chat_container = $('#chat-input-container')
    @$chat_form = $('#chat-form')
    @$chat_input = $('#chat-input')
    @$silence = $('#silence')
    @$silence_container = $('#silence-container')
    @$ballot = $('#ballot')

    @$silence.off('click').on('click', =>
      @$silence_container.hide()
      @$chat_container.hide()
      @$enquete.show()

      init_timer(60)

      @$ballot.off('click').on('click', =>
        @$enquete.find('input').attr('disabled', 'disabled')
        @$ballot.addClass('disabled').text('集計中...')
        @$ballot.off('click')
        false
      )
      false
    )

    @$chat_form.on('submit', =>
      message = @$chat_input.val()
      return false if message.length <= 0
      ss.rpc('room.add_message', message)
      @$chat_input.val('')
      false
    )

  new_message: (message)=>
    return if !@message or !@$chat
    message = @message message
    @$chat.prepend(message)

ss.event.on('room.add_message', (new_message)=>
  return if !(router?.pages?.room?)
  router.pages.room.new_message(new_message)
)