mongoose = require('mongoose')
Schema = mongoose.Schema

MessageSchema = new Schema
  speaker: {type: Schema.ObjectId, ref: 'User'}
  message: String
  stage: Number
  created_at: Date
  updated_at: Date

MessageSchema.pre 'save', (next)->
  @updated_at = (new Date()).getTime()
  next()

mongoose.model 'Message', MessageSchema

module.exports.Message = mongoose.model 'Message'
module.exports.MessageSchema = MessageSchema