window.App =
  views: {}
  models: {}
  collections: {}

class App.Page extends Backbone.View
  el: '#main'
  template: ''
  params: {}
  render: =>
    if @template? and @template.length > 0
      @$el.empty().html(ss.tmpl[@template].render(@params))

App.require = (scripts)->
  args = arguments
  return App.require(args) if args.length > 1
  return require(scripts) if typeof(scripts) is 'string'
  require('/'+script) for script in scripts