App.require([
  'views/lobby'
  'views/room'
  'views/create'
  'views/not_found'
])
class router extends Backbone.Router
  $html: null
  pages: {}

  routes:
    '': 'lobby'
    'lobby': 'lobby'
    'room': 'room'
    'create': 'create'
    '*not_found': 'not_found'

  reset: =>
    @$html = $('html') if @$html?.length? and @$html.length > 0
    return if @$html is null or (@$html?.length? and @$html.length is 0)
    @$html.removeClass('dark') if @$html?.removeClass?

  move: (name)=>
    @navigate(name, trigger: true)

  show: (page)=>
    @reset()
    @pages[page] = new App.views[page]() if !@pages[page]?
    @pages[page].render()

  lobby: =>
    @show 'lobby'

  create: =>
    @show 'create'

  room: =>
    @show 'room'

  not_found: =>
    @show 'not_found'

window.router = new router()
Backbone.history.start()