mongoose = require('mongoose')
Schema = mongoose.Schema

UserSchema = new Schema
  id_str: String
  name: String
  icon: String
  room: {type: Schema.ObjectId, ref: 'Room'}
  created_at: {type: Date, default: Date.now }
  updated_at: {type: Date, default: Date.now }

UserSchema.pre 'save', (next)->
  @updated_at = (new Date()).getTime()
  next()

mongoose.model 'User', UserSchema

module.exports.User = mongoose.model 'User'
module.exports.UserSchema = UserSchema