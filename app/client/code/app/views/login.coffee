class App.views.login extends Backbone.View
  el: '#login-info'
  login_btn: '<a href="/auth/twitter" class="btn btn-info">Twitterでログインする</a>'
  user_info: _.template '''
  <ul class="nav">
    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <img src="<%= icon %>" class="user-icon-mini"> <%= name %>
      </a>
      <ul class="dropdown-menu">
        <li><a href="#logout">logout</a></li>
      </ul>
    </li>
  </ul>
  '''

  initialize: =>
    ss.rpc 'user.get_info', (user)=>
      console.log(user)
      if user is null
        @$el.html(@login_btn)
      else
        @$el.html(@user_info(user))