exports.actions = (req, res, ss) ->
  req.use('session')

  User = require('./models/user').User

  get_info: =>
    user = null
    if req?.session?.user?.name? and req.session.user.icon?
      user =
        name: req.session.user.name
        icon: req.session.user.icon
    res(user)

  is_loggedin: =>
    res req?.session?.user?
