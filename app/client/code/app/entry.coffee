window.ss = require('socketstream')

require('/init')
require('/views/login')

ss.server.on 'disconnect', ->
  console.log('Connection down :-(')

ss.server.on 'reconnect', ->
  console.log('Connection back up :-)')

ss.server.on 'ready', ->
  $ ->
    require('/router')
    new App.views.login()