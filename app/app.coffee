http = require('http')
ss = require('socketstream')
everyauth = require('everyauth')
config = require('config')
mongoose = require('mongoose')

mongoose.set('debug', true)

#connect to mongodb
mongoose.connect 'mongodb://localhost/AreYouWereWolf'

ss.session.store.use('redis')
ss.publish.transport.use('redis')

#Define a single-page client called 'main'
ss.client.define 'main',
  view: 'app.html'
  css:  ['app.less', 'dark.less']
  code: ['libs/jquery.min.js', 'libs/underscore-min.js', 'libs/backbone-min.js', 'app']
  tmpl: '*'

#Serve this client on the root URL
ss.http.route '/', (req, res)->
  res.serveClient('main')

User = require('./server/rpc/models/user').User

#twitter oauth
# /auth/twitter <- everyauthがええ感じにやってくれる。けど固定URL
everyauth.twitter
  .consumerKey(config.Twitter.consumerKey)
  .consumerSecret(config.Twitter.consumerSecret)
  .findOrCreateUser((session, accessToken, accessTokenSecret, twitterUserMetaData)->
    #str_id でユーザーを検索
    User.findOne({id_str: twitterUserMetaData.id_str}, (err, user)->

      #create new user
      user = new User id_str: twitterUserMetaData.id_str if !user

      #create or update user name and icon
      user.name = twitterUserMetaData.screen_name
      user.icon = twitterUserMetaData.profile_image_url

      #save user
      user.save (err, u)->
        session.userId = u._id
        session.user = user.toObject()
        session.save()

      true
    )
    true
  )
  .redirectPath('/')

#Code Formatters
ss.client.formatters.add(require('ss-coffee'))
ss.client.formatters.add(require('ss-less'))

#Use server-side compiled Hogan (Mustache) templates. Others engines available
ss.client.templateEngine.use(require('ss-hogan'))

#Minimize and pack assets if you type: SS_ENV=production node app.js
if ss.env is 'production'
  ss.client.packAssets()

#for everyauth
ss.http.middleware.prepend(ss.http.connect.bodyParser())
ss.http.middleware.append(everyauth.middleware())

#Start web server
server = http.Server(ss.http.middleware)
server.listen(3000)

#Start SocketStream
ss.start(server)