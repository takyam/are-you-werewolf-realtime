mongoose = require('mongoose')
Schema = mongoose.Schema

MemberSchema = new Schema
  user: {type: Schema.ObjectId, ref: 'User'}
  is_dead: {type: Boolean, default: false}
  job: {type: Number, default: 0}
  is_organizer: {type: Boolean, default: false}

mongoose.model 'Member', MemberSchema

module.exports.Member = mongoose.model 'Member'
module.exports.MemberSchema = MemberSchema