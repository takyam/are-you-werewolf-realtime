class App.views.lobby extends App.Page
  template: 'lobby-index'

  row: _.template '''
       <tr>
         <td><%= name %></td>
         <td><%= users.length %>人/<%= limit %>人</td>
         <td><%= status %></td>
         <td>
           <a href="#room" class="btn" data-id="<%= _id %>">参加する</a>
         </td>
       </tr>
       '''

  render: =>
    super
    @$table = $('#lobby-table')
    @$tbody = @$table.find('tbody')
    ss.rpc 'room.get_rooms_list', (rooms)=>
      html = ''
      html += @row(room) for room in rooms
      @$tbody.html(html)

    @$btn_container = $('#lobby-create-container')
    ss.rpc 'user.is_loggedin', (is_loggedin)=>
      if is_loggedin
        @$btn_container.show()
      else
        @$btn_container.hide()