# Server-side Code

# Define actions which can be called from the client using ss.rpc('demo.ACTIONNAME', param1, param2...)
exports.actions = (req, res, ss) ->

  # Example of pre-loading sessions into req.session using internal middleware
  req.use('session')

  # Uncomment line below to use the middleware defined in server/middleware/example
  #req.use('example.authenticated')

  mongoose = require('mongoose')
  Room = require('./models/room').Room
  User = require('./models/user').User
  Member = require('./models/member').Member
  Message = require('./models/message').Message

  get_rooms_list: =>
    console.log(req.session.user.room)
    Room.find({}, {
      name:1
      desc:1
      limit:1
      time:1
      'organizer.name':1
      'organizer.icon':1
      'users.name':1
      'users.icon':1
      status:1
      created_at:1
    }, (err, rooms)=>
      res(rooms)
    )

  get_name: =>
    res(req.session.user.name)

  create: (params) =>
    errors =
      all: []
      name: []
      desc: []
      limit: []
      time:
        date: []
        night: []

    #ログインチェック
    if !(req.session.user)
      errors.all.push 'ログインしていないと村を作成することはできません'
      return res(false, errors)
    else if req.session.user.room?
      errors.all.push '既に他の村に参加しているため作成することができません'
      return res(false, errors)

    #TODO : Validatorってどう作るのが正解なんだろ。server/middleware??
    #とりあえずゴリッとしておくよ。

    if !(params?) or typeof(params) isnt 'object' or !(params.time?)
      errors.all.push '不正なデータです'
    else
      #名前のバリデーション
      if !(params.name?) or params.name.length <= 0
        errors.name.push '入力必須項目です'
      else
        errors.name.push '100文字以内で入力してください' if params.name.length >= 100

      if !(params.limit?)
        errors.limit.push '入力必須項目です'
      else
        limit = parseInt(params.limit, 10)
        if limit.isNaN
          errors.limit.push '半角数字で入力してください'
        else if limit < 5 or limit > 30
          errors.limit.push '5人〜30人の範囲で選んでください'

      if !(params.time.date?)
        errors.time.date.push '入力必須項目です'
      else
        date = parseInt(params.time.date, 10)
        if date.isNaN
          errors.time.date.push '半角数字で入力してください'
        else if limit < 1 or limit > 30
          errors.time.date.push '1分〜30分の範囲で選んでください'

      if !(params.time.night?)
        errors.time.night.push '入力必須項目です'
      else
        night = parseInt(params.time.night, 10)
        if night.isNaN
          errors.time.night.push '半角数字で入力してください'
        else if limit < 1 or limit > 30
          errors.time.night.push '1分〜30分の範囲で選んでください'

    if errors.all.lenght > 0 or errors.name.length > 0 or errors.desc.length > 0 or errors.limit.length > 0 or errors.time.date.length > 0 or errors.time.night.length > 0
      return res(false, errors)

    User.findOne {id_str: req.session.user.id_str}, (err, user)=>
      if err isnt null
        errors.all.push 'ユーザデータの取得に失敗しました'
        return res(false, errors)

      member = new Member
        user: user
        is_organizer: true
      room = new Room
        name: params.name
        desc: params.desc
        limit: params.limit
        time:
          date: params.time.date
          night: params.time.night
        organizer: user
        users: [member]
      room.save (err)->
        if err isnt null
          errors.all.push '何らかの事情で村の作成に失敗しました'
          return res(false, errors)

        user.room = room
        user.save (err)->
          if err isnt null
            errors.all.push '何らかの事情で村の作成に失敗しました'
            room.remove()
            return res(false, errors)
          res(true)

  add_message: (message_text)=>
    return if !(req.session?.user?.id_str?)
    User.findOne({id_str: req.session.user.id_str}).populate('room').exec (err, user)=>
      return if err isnt null or !(user?.room?)

      message = new Message
        speaker: user
        stage: user.room.current_stage
        message: message_text

      message.save (err, msg)->
        #$push がmethodとしてないので UPDATE かけるお(^ρ^)
        user.room.update(
          {$push: {'messages': msg }},
          (err, result)->
            user_ids = []
            user_ids.push u.user for u in user.room.users

            ss.publish.user user_ids, 'room.add_message',
              name: user.name
              icon: user.icon
              message: msg.message
        )


